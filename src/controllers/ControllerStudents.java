package controllers;

import database.DataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

/**
 * Класс контроллер для окна заполнения данных таблицы students
 * @author М. Лебедев
 */
public class ControllerStudents {

    public TextField student_code;
    public TextField full_name_student;
    public TextField date_of_birth;
    public TextField home_address;
    public TextField contact_number;

    private final DataBase dataBase = new DataBase();

    /**
     * При нажатие на кнопку происходит передача данных из полей окна в запрос в таблицу students
     * @throws SQLException ошибка в обращение к базе данных
     */
    @FXML
    void insertData() throws SQLException {
        dataBase.insertIntoStudents(Integer.parseInt(student_code.getText()), full_name_student.getText(),
                date_of_birth.getText(), home_address.getText(), contact_number.getText());
    }
}
