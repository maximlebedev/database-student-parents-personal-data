package controllers;

import database.DataBase;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

/**
 * Класс контроллер для окна заполнения данных таблицы personal_data
 * @author М. Лебедев
 */
public class ControllerPersonalData {

    private final DataBase dataBase = new DataBase();
    public TextField student_code;
    public TextField passport_data;
    public TextField TIN_number_SNILS;

    /**
     * При нажатие на кнопку происходит передача данных из полей окна в запрос в таблицу personal_data
     * @throws SQLException ошибка в обращение к базе данных
     */
    @FXML
    public void insertData() throws SQLException {
        dataBase.insertIntoPersonalData(Integer.parseInt(student_code.getText()), Integer.parseInt(passport_data.getText()), Integer.parseInt(TIN_number_SNILS.getText()));
    }
}
