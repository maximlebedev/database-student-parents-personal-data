package controllers;

import database.DataBase;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.MapValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Класс контроллер для главного окна приложения
 *
 * @author М. Лебедев
 */
public class Controller {

    public TableView<HashMap> tableView;
    public ComboBox<String> tableComboBox;
    public Button buttonAdd;

    private final DataBase dataBase = new DataBase();

    /**
     * Заполнение ComboBox'а списком таблиц из БД
     *
     * @throws SQLException ошибка в обращение к базе данных
     */
    @FXML
    void initialize() throws SQLException {
        fillComboBox();
    }

    private void fillComboBox() throws SQLException {
        tableComboBox.setPromptText("Выбор таблицы...");
        ArrayList<String> tables = dataBase.getTableList();
        tableComboBox.setItems(FXCollections.observableArrayList(tables));
    }

    /**
     * Заполнение TableView на главном окне
     *
     * @throws SQLException ошибка в обращение к базе данных
     */
    @FXML
    void fillTableView() throws SQLException {
        tableView.getColumns().clear();

        ArrayList<String> columnList = dataBase.getColumnList(tableComboBox.getValue());
        ArrayList<TableColumn> tableColumns = createColumns(columnList);

        setItemsInTableView(columnList, tableComboBox.getValue());
        setValuesColumns(tableColumns, columnList);
        addColumnsIntoTableView(tableColumns);
    }

    private ArrayList<TableColumn> createColumns(ArrayList<String> columnList) {
        ArrayList<TableColumn> tableColumns = new ArrayList<>();
        for (String s : columnList) {
            TableColumn tableColumn = new TableColumn<HashMap, Object>(s);
            tableColumn.setStyle("-fx-alignment: CENTER");
            tableColumns.add(tableColumn);
        }

        return tableColumns;
    }

    private void setItemsInTableView(ArrayList<String> columnNames, String tableName) throws SQLException {
        tableView.setItems(dataBase.getInfo(tableName, dataBase.getColumnCount(tableName), columnNames));
    }

    private void setValuesColumns(ArrayList<TableColumn> tableColumns, ArrayList<String> columnNames) {
        for (int i = 0; i < tableColumns.size(); i++)
            tableColumns.get(i).setCellValueFactory(new MapValueFactory<String>(columnNames.get(i)));
    }

    private void addColumnsIntoTableView(ArrayList<TableColumn> tableColumns) {
        for (TableColumn tableColumn : tableColumns) {
            tableView.getColumns().add(tableColumn);
        }
    }

    /**
     * Открытие окна заполнения данных
     *
     * @throws IOException ошибка ввода/вывода
     */
    @FXML
    void add() throws IOException {
        if(tableComboBox.getValue() != null) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/" + tableComboBox.getValue() + ".fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Добавление");
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(root));
            stage.show();
        }
    }
}
